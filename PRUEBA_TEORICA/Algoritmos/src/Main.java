import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese la cadena de letras separadas por '|': ");
        String inputString = scanner.nextLine();

        try {
            String result = processSubstrings(inputString);
            System.out.println("Cadena Resultante: " + result);
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static String processSubstrings(String inputString) {
        StringBuilder finalResult = new StringBuilder();

        // Dividir la cadena de entrada en subcadenas separadas por "|"
        String[] substrings = inputString.split("\\|");

        // Verificar si hay 3 o 4 subcadenas
        if (substrings.length < 3 || substrings.length > 4) {
            throw new IllegalArgumentException("La cadena no contiene 3 o 4 subcadenas separadas por '|'. Número de subcadenas: " + substrings.length);
        }

        for (String substring : substrings) {
            validateSubstring(substring);
        }

        for (int i = 0; i < substrings.length; i++) {
            String currentSub = substrings[i];
            String oppositeSub = substrings[substrings.length - 1 - i];
            StringBuilder resultSub = new StringBuilder();

            // Encontrar las letras repetidas en ambas subcadenas
            Set<Character> repeatedLetters = new HashSet<>();
            for (char letter : currentSub.toCharArray()) {
                if (oppositeSub.indexOf(letter) != -1) {
                    repeatedLetters.add(letter);
                }
            }

            // Encontrar la mayor posición de la letra repetida en la subcadena opuesta
            int maxPos = -1;
            for (char letter : repeatedLetters) {
                int pos = oppositeSub.indexOf(letter);
                if (pos > maxPos) {
                    maxPos = pos;
                }
            }

            // Construir la subcadena resultante eliminando letras repetidas
            for (char letter : currentSub.toCharArray()) {
                if (!repeatedLetters.contains(letter) || oppositeSub.indexOf(letter) == maxPos) {
                    resultSub.append(letter);
                }
            }

            resultSub.append('*');

            for (char letter : oppositeSub.toCharArray()) {
                if (!repeatedLetters.contains(letter) || oppositeSub.indexOf(letter) == maxPos) {
                    resultSub.append(letter);
                }
            }

            finalResult.append(resultSub).append("|");
        }

        // Eliminar el último '|' de la cadena final
        finalResult.deleteCharAt(finalResult.length() - 1);
        return finalResult.toString();
    }

    public static void validateSubstring(String substring) {
        // Verificar que la subcadena cumpla con las condiciones
        if (substring.length() != 5 || !substring.matches("[a-zA-Z]+")) {
            throw new IllegalArgumentException("Subcadena " + substring + " no cumple con el formato requerido: Longitud inválida o contiene caracteres no alfabéticos.");
        }

        // Verificar que la subcadena no contenga más de 2 veces la misma letra
        for (char letter : substring.toCharArray()) {
            if (substring.chars().filter(ch -> ch == letter).count() > 2) {
                throw new IllegalArgumentException("Subcadena " + substring + " contiene caracteres repetidos más de 2 veces.");
            }
        }
    }
}