import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { hasAdminRole } from './utils';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor( private router: Router) {}

  canActivate(): boolean {
    if (hasAdminRole()) {
      return true;
    } else {
      // Redirige a la página de inicio de sesión u otra página adecuada
      this.router.navigate(['/login']);
      return false;
    }
  }
}