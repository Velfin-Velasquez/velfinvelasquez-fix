import { LOCAL_STORAGE } from "../enums/localstorage";

export const getTokenLocalStorage = () => {
    const token = localStorage.getItem(LOCAL_STORAGE.USER);
    return token;
}

export const removeTokenLocalStorage = () => {
    const token = localStorage.removeItem(LOCAL_STORAGE.USER);
    return token;
}

export const getRoles = (): string[] => {
    const token = getTokenLocalStorage();
    if (token) {
        const decodedToken = JSON.parse(atob(token.split('.')[1]));
        const authorities = decodedToken.authorities;
        if (Array.isArray(authorities)) {
            return authorities.map((authority: any) => authority.authority);
        }
    }
    return [];
}

export const hasAdminRole = (): boolean => {
    const token = getTokenLocalStorage();
    if (token) {
        const decodedToken = JSON.parse(atob(token.split('.')[1]));
        const authorities = decodedToken.authorities;
        if (Array.isArray(authorities)) {
            return authorities.some((authority: any) => authority.authority === 'ADMIN');
        }
    }
    return false;
}
