import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { LOCAL_STORAGE } from 'src/app/enums/localstorage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loading: boolean = false;
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private loginService: LoginService
  ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  login() {
    this.loading = true;
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;

    this.loginService.login(email, password).subscribe(
      (data) => {
        this.loading = false;
        localStorage.setItem(LOCAL_STORAGE.USER, data.token);
        this.toastr.success("Login exitoso!");
        this.router.navigate(['/dashboard']);

      },
      (error) => {
        this.loading = false;
        this.toastr.error(error.error.message)
        console.error('Error al obtener los datos desde la API:', error);
      }
    );


  }

  ngOnInit(): void { 
    if (localStorage.getItem(LOCAL_STORAGE.USER)) {
      this.router.navigate(['/dashboard']);
    }
  }
}
