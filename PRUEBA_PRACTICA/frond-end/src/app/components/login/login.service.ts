import { Injectable } from '@angular/core';
import { environment } from './../../../environment/environment'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private apiUrl = `${environment.BASE_URL}/api/auth/singin`; 

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    const requestBody = {
      userOrEmail: email,
      password: password
    }
    return this.http.post<any>(this.apiUrl, requestBody);
  }
}
