import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from 'src/app/enums/localstorage';

@Component({
  selector: 'app-sing-up',
  templateUrl: './sing-up.component.html',
  styleUrls: ['./sing-up.component.css']
})
export class SingUpComponent implements OnInit {

  registerUser: FormGroup
  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService) {
    this.registerUser = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required,Validators.minLength(10)]],
      repeatPassword: ['', Validators.required],
    });
  }

  register() {
    // this.loading = true;
    const email = this.registerUser.value.email;
    const password = this.registerUser.value.password;
    const repeatPassword = this.registerUser.value.repeatPassword;


    if (password !== repeatPassword) {
      this.toastr.error("passwords do not match!");
      return;
    }
    if (!this.registerUser.valid) {
      return;
    }
    

  }

  ngOnInit(): void { 
    if (localStorage.getItem(LOCAL_STORAGE.USER)) {
      this.router.navigate(['/dashboard']);
    }
  }


}
