import jwt_decode from 'jwt-decode';
import { Component, OnInit } from '@angular/core';
import { UpdateEventService } from '../update-event/update-event.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from '../profile/profile.service';
import { getTokenLocalStorage } from 'src/app/utils';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {
  evento: any = {};
  users: any = [];
  loading: boolean = false;
  user: any = {};

  constructor(private profileService: ProfileService, private router: Router, private route: ActivatedRoute, private toastr: ToastrService, private updateEventService: UpdateEventService) { }
  ngOnInit(): void {


    try {
      const decodedToken: any = jwt_decode(getTokenLocalStorage() || '');
      const user = decodedToken.sub ?? '';
      this.getuserByUsername(user);

    } catch (error) {
      this.toastr.error('hubo un error');
    }

    this.route.params.subscribe(params => {
      const id = params['id'];
      this.getEventById(id);
    });



  }

  getuserByUsername(username: string) {
    this.loading = true;
    this.profileService.getUserByUsername(username)
      .subscribe(
        user => {
          this.loading = false;
          this.user = user;
        },
        err => {
          this.loading = false;
          this.toastr.error("Hubo un error");
        });
  }

  getEventById(id: any) {
    this.updateEventService.getEventById(id)
      .subscribe(
        event => {

          this.evento = event;
          this.users = event.users;

        },
        err => {
          this.toastr.error("Hubo un error");
        });
  }

  goToByTicket() {
    this.router.navigate([`/by-ticket/${this.evento.id}/${this.user.id}`]);
  }


}
