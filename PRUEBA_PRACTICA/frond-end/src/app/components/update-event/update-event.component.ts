import { UpdateEventService } from './update-event.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.css']
})
export class UpdateEventComponent implements OnInit {
  loading: boolean = false;

  UpateEventData: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private updateEventService: UpdateEventService,
    private toastr: ToastrService) {
    this.UpateEventData = this.fb.group({
      name: ['', [Validators.required]],
      dayEvent: ['', [Validators.required]],
      tickes: ['', [Validators.min(1)]],
      price: ['', [Validators.min(0.1)]],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params['id'];
      const data = this.getEventById(id);
    });

  }

  updateEvent() {
    this.loading = true
    this.route.params.subscribe(params => {
      const id = params['id'];
      const name = this.UpateEventData.value.name;
      const dayEvent = this.UpateEventData.value.dayEvent;
      const tickes = this.UpateEventData.value.tickes;
      const price = this.UpateEventData.value.price;
      const requestBody = {
        name,
        dayEvent,
        price,
        tickes,
      }

      this.updateEventService.updateTikets(requestBody, id)
        .subscribe(data => {
          this.loading=false;
          this.toastr.success('Evento Actualizado');
          this.router.navigate(['/list-events']);
          
        }, err => {
          this.loading=false;
          this.toastr.error('Hubo un error');
          
        });
    });



  }

  getEventById(id: any) {
    this.updateEventService.getEventById(id)
      .subscribe(
        event => {

          this.UpateEventData.patchValue({
            name: event.name,
            dayEvent: event.dayEvent,
            tickes: event.tickes,
            price: event.price,
          });
        },
        err => {

        });
  }

}
