import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { getTokenLocalStorage } from 'src/app/utils';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  private apiUrl = `${environment.BASE_URL}/api/events`;

  constructor(private http: HttpClient) { }

  createTikets(name: string, dayEvent: Date, price: any, tickes: any): Observable<any> {
    const requestBody = {
      name,
      dayEvent,
      price,
      tickes,
    }
    const token = getTokenLocalStorage();

    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${token}`
      })
    };
    return this.http.post<any>(this.apiUrl, requestBody, httpOptions);
  }
}
