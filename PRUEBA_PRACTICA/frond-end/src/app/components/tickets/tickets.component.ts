import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TicketsService } from './tickets.service';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent {
  registerEvent: FormGroup
  constructor(
    private ticketService: TicketsService,
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService) {
    this.registerEvent = this.fb.group({
      name: ['', [Validators.required]],
      dayEvent: ['', [Validators.required]],
      tickes: ['', [Validators.min(1)]],
      price: ['', [Validators.min(0.1)]],
    });
  }

  loading: boolean = false;

  createTikect() {
  this.loading=true;
    const name = this.registerEvent.value.name;
    const dayEvent = this.registerEvent.value.dayEvent;
    const tickes = this.registerEvent.value.tickes;
    const price = this.registerEvent.value.price;

    this.ticketService.createTikets(name,dayEvent,price,tickes).subscribe(
      (data) => {
        this.loading = false;
        this.toastr.success("Evento creado!");
        this.router.navigate(['/dashboard']);

      },
      (error) => {
        this.loading = false;
        this.toastr.error(error.error.message)
        console.error('Error al obtener los datos desde la API:', error);
      }
    );
  }
}
