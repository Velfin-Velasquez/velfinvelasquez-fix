import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketsService } from './tickets.service';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [TicketsService],
})
export class TicketsModule { }
