import { Component, OnInit } from '@angular/core';
import jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
import { getTokenLocalStorage, hasAdminRole } from 'src/app/utils';
import { ProfileService } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loading: boolean = false;
  user: any = {};
  roles: any = [];

  constructor(private toastr: ToastrService,
    private profileService: ProfileService) {

  }

  ngOnInit(): void {
    
    try {
      const decodedToken: any = jwt_decode(getTokenLocalStorage() || '');
      const user = decodedToken.sub ?? '';
      this.getuserByUsername(user);

    } catch (error) {
      this.toastr.error('hubo un error');
    }
  }

  getuserByUsername(username: string) {
    this.loading = true;
    this.profileService.getUserByUsername(username)
      .subscribe(
        user => {
          this.loading = false;
          this.user = user;
          this.roles = user.roles;
        },
        err => {
          this.loading = false;
          this.toastr.error("Hubo un error");
        });
  }


}
