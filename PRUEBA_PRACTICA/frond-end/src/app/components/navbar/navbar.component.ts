import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { removeTokenLocalStorage } from 'src/app/utils';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(private router: Router) {
    
  }

  cerrarSesion(){
    removeTokenLocalStorage();
    this.router.navigate(['/login']);
  }
}
