import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BuyTicketService } from './buy-ticket.service';
import { ProfileService } from '../profile/profile.service';
import { EventsService } from '../events/events.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [BuyTicketService, ProfileService, EventsService]
})
export class BuyTicketModule { }
