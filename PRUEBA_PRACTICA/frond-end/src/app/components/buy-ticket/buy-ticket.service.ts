import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { getTokenLocalStorage } from 'src/app/utils';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class BuyTicketService {

  private apiUrl = `${environment.BASE_URL}/api/events`;

  constructor(private http: HttpClient) { }

  buyTikets(id_event: string, id_user: string): Observable<any> {

    const token = getTokenLocalStorage();

    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${token}`
      })
    };
    return this.http.get<any>(`${this.apiUrl}/buy/${id_user}/${id_event}`, httpOptions);
  }
}
