import { Component, OnInit } from '@angular/core';
import { UpdateEventService } from '../update-event/update-event.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { BuyTicketService } from './buy-ticket.service';

@Component({
  selector: 'app-buy-ticket',
  templateUrl: './buy-ticket.component.html',
  styleUrls: ['./buy-ticket.component.css']
})
export class BuyTicketComponent implements OnInit {
  evento: any = [];
  id_event: string = '';
  id_user: string = '';
  constructor(private router:Router,private route: ActivatedRoute, private toastr: ToastrService, private updateEventService: UpdateEventService, private buyService: BuyTicketService) { }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id_event = params['id_event'];
      const id_user = params['id_user'];
      this.id_event = id_event;
      this.id_user = id_user;

      this.getEventById(id_event);
    });


  }

  getEventById(id: any) {
    this.updateEventService.getEventById(id)
      .subscribe(
        event => {

          this.evento = event;

        },
        err => {
          this.toastr.error("Hubo un error");
        });
  }

  buy() {
    console.log(this.id_event);
    console.log(this.id_user);
    this.buyService.buyTikets(this.id_event, this.id_user).subscribe(
      (data) => {
        this.toastr.success(data.message);
        this.router.navigate(['/list-events']);
      },
      err => {
        this.toastr.error("Ya has comprado este ticket");
        this.router.navigate(['/list-events']);
      }
    );
  }

}
