import { Component, OnDestroy, OnInit } from '@angular/core';
import { EventsService } from './events.service';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { hasAdminRole } from 'src/app/utils';


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  isAdmin:boolean=false;
  loading: boolean = false;
  allEvents: any = [];


  constructor(private eventsService: EventsService, private router: Router) { }

  ngOnInit(): void {
    this.isAdmin = hasAdminRole();
    this.getAllEvents();
  }



  getAllEvents() {
    this.eventsService.getAllEvents().subscribe((events) => {
      this.allEvents = events;
    }, (err) => {
      this.allEvents = [];
      
    });
  }

  edit(id: any) {
    this.router.navigate([`update-event/${id}`])
  }
  delete(id: any) { }
  
  moreInfo(id: any) {
    this.router.navigate([`event-detail/${id}`]);
  }
}