import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { LOCAL_STORAGE } from './enums/localstorage';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree {

    const userToken = localStorage.getItem(LOCAL_STORAGE.USER);

    if (userToken && userToken !== 'undefined') {
      // Si existe un token válido, se permite el acceso a la ruta
      return true;
    } else {
      // Si no existe un token válido, se redirige a la página de inicio de sesión
      return this.router.createUrlTree(['/login']);
    }
  }
}