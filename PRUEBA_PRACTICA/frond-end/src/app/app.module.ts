import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent, LoginComponent, SingUpComponent } from './components';
import { SpinnerComponent } from './shared';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TicketsComponent } from './components/tickets/tickets.component';
import { DataTablesModule } from 'angular-datatables';
import { EventsComponent } from './components/events/events.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UpdateEventComponent } from './components/update-event/update-event.component';
import { EventDetailsComponent } from './components/event-details/event-details.component';
import { BuyTicketComponent } from './components/buy-ticket/buy-ticket.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    SingUpComponent,
    SpinnerComponent,
    NavbarComponent,
    ProfileComponent, 
    UpdateEventComponent,
    TicketsComponent,
    EventsComponent,
    EventDetailsComponent,
    BuyTicketComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DataTablesModule,
    FontAwesomeModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
