import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { DashboardComponent, LoginComponent, SingUpComponent } from './components';
import { EventDetailsComponent } from './components/event-details/event-details.component';
import { EventsComponent } from './components/events/events.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TicketsComponent } from './components/tickets/tickets.component';
import { UpdateEventComponent } from './components/update-event/update-event.component';
import { AdminGuard } from './admin.guard';
import { BuyTicketComponent } from './components/buy-ticket/buy-ticket.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'sing-up', component: SingUpComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard], },
  { path: 'tickes', component: TicketsComponent, canActivate: [AuthGuard, AdminGuard], },
  { path: 'list-events', component: EventsComponent, canActivate: [AuthGuard], },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], },
  { path: 'update-event/:id', component: UpdateEventComponent, canActivate: [AuthGuard, AdminGuard], },
  { path: 'event-detail/:id', component: EventDetailsComponent, canActivate: [AuthGuard], },
  { path: 'by-ticket/:id_event/:id_user', component: BuyTicketComponent, canActivate: [AuthGuard], },
  { path: "**", redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
