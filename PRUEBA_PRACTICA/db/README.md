# Levantar la base de datos de Postgres con docker

    
1) Configure sus credenciales de la base de datos en el archivo **.env**
```
POSTGRES_USER=velkin
POSTGRES_PASSWORD=password
POSTGRES_DB=prueba_tec
```

2) Levante el servicio con
```bash
  docker-compose up -d
```

3) Para bajar el servicio
```bash
  docker-compose down
```
## En el cliente de la base de datos ejecute el procedimiento almacenado

```
CREATE OR REPLACE FUNCTION update_tickets_on_insert()
RETURNS TRIGGER AS $$
BEGIN
  -- Restar un ticket al evento
  UPDATE events SET tickes = tickes - 1 WHERE id = NEW.event_id;
  
  -- Verificar si el evento tiene tickets disponibles
  IF (SELECT tickes FROM events WHERE id = NEW.event_id) >= 0 THEN
    RETURN NEW;
  ELSE
    -- Si no hay tickets disponibles, cancelar la inserción del evento
    RAISE EXCEPTION 'No hay tickets disponibles para este evento';
    RETURN NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;

-- Crear un trigger que se ejecutará antes de insertar en la tabla users_events
CREATE TRIGGER before_insert_users_events
BEFORE INSERT ON users_events
FOR EACH ROW
EXECUTE FUNCTION update_tickets_on_insert();
```

