package prueba_tecnica_spring.controller;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import prueba_tecnica_spring.models.EventModel;
import prueba_tecnica_spring.service.EventServiceImp;
import prueba_tecnica_spring.util.ResponseMessage;

import java.util.List;

@RestController
@RequestMapping("/api/events")
public class EventsController {
    private final EventServiceImp eventService;

    public EventsController(EventServiceImp eventService) {
        this.eventService = eventService;
    }

    @GetMapping()
    public List<EventModel> getAll() {
        return eventService.getAll();
    }

    @GetMapping("/{id}")
    public EventModel getById(@PathVariable Long id) {
        return eventService.getById(id);
    }

    @PostMapping()
    public ResponseEntity<?> savePerson(@Valid @RequestBody EventModel event, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ResponseMessage(bindingResult.getFieldError().getDefaultMessage()));
        }

        return new ResponseEntity<>(eventService.save(event), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody EventModel event) {
        EventModel userUpdated = eventService.update(id, event);
        return ResponseEntity.ok(userUpdated);
    }

    @GetMapping("/buy/{id_user}/{id_event}")
    public ResponseEntity<?> byTicket(@PathVariable Long id_user,@PathVariable Long id_event) {
        String userUpdated = eventService.byTicket(id_user, id_event);
        return ResponseEntity.ok(new ResponseMessage(userUpdated));
    }
}
