package prueba_tecnica_spring.controller;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import prueba_tecnica_spring.models.SessionModel;
import prueba_tecnica_spring.models.UserModel;
import prueba_tecnica_spring.repository.SessionRepository;
import prueba_tecnica_spring.repository.UserRepository;
import prueba_tecnica_spring.security.JwtUtilService;
import prueba_tecnica_spring.service.UserServiceImpl;
import prueba_tecnica_spring.util.PasswordUtils;
import prueba_tecnica_spring.util.ResponseMessage;
import prueba_tecnica_spring.util.UserResponse;

/**
 * <b>Author:</b> Velfin Velasquez <br>
 * <b>Description:</b>  AuthController Controller<br>
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final UserRepository userRepository;
    private final  JwtUtilService jwtUtilService;
    private final SessionRepository sessionRepo;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService usuarioDetailsService;
    private final UserServiceImpl userService;

    @Autowired
    public AuthController(JwtUtilService jwtUtilService, UserRepository userRepository , SessionRepository sessionRepo, UserServiceImpl userService, UserDetailsService usuarioDetailsService, AuthenticationManager authenticationManager){
        this.userService=userService;
        this.usuarioDetailsService=usuarioDetailsService;
        this.authenticationManager=authenticationManager;
        this.sessionRepo=sessionRepo;
        this.userRepository=userRepository;
        this.jwtUtilService=jwtUtilService;
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(@RequestBody Map<String, String> data) {
        String userOrEmail = data.get("userOrEmail");
        UserModel userSingOut = userService.logout(userOrEmail);
        if (userSingOut != null) {
            return ResponseEntity.ok(new ResponseMessage("Cerraste Sesion con exito!"));
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage("No existe este usuario"));
        }

    }

    @GetMapping("/logoutAll")
    public ResponseEntity<?> logoutAll(@RequestBody Map<String, String> data) {
        String mesagge = userService.logoutAllUser();
        return ResponseEntity.ok(new ResponseMessage(mesagge));
    }

    @PostMapping("/singin")
    public ResponseEntity<?> login(@RequestBody Map<String, String> loginData) {
        String userOrEmail = loginData.get("userOrEmail");
        String password = loginData.get("password");
        UserModel user = new UserModel();
        user = userService.getUserByUserOrEmail(userOrEmail);

        if (user != null && PasswordUtils.isPasswordValid(password, user.getPassword())) {
            if (!user.getStatus()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new ResponseMessage("Usuario bloqueado por seguridad, contacte con administrador!"));
            }
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userOrEmail, password));
            final UserDetails userDetails = usuarioDetailsService.loadUserByUsername(userOrEmail);

            String jwt = jwtUtilService.generateToken(userDetails);

            UserResponse response = new UserResponse(user, jwt);

            SessionModel session = new SessionModel();
            session.setFechaInicio(new Date());
            session.setUser(user);
            sessionRepo.save(session);

            user.setSession_active(true);
            userRepository.save(user);
            return ResponseEntity.ok(response);

        } else {


            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage("Usuario o clave invalida"));
        }

    }
}
