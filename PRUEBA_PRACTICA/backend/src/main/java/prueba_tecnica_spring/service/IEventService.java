package prueba_tecnica_spring.service;

import prueba_tecnica_spring.models.EventModel;

import java.util.List;

public interface IEventService {
    List<EventModel> getAll();
    EventModel getById(Long id);
    EventModel save(EventModel eventModel);
    EventModel update(Long id,EventModel event);

     String byTicket(Long userId, Long eventId);
}
