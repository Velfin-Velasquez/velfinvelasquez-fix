package prueba_tecnica_spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import prueba_tecnica_spring.exeptions.ResourceNotFoundException;
import prueba_tecnica_spring.models.EventModel;
import prueba_tecnica_spring.models.PersonModel;
import prueba_tecnica_spring.models.UserModel;
import prueba_tecnica_spring.repository.EventRepository;
import prueba_tecnica_spring.repository.UserRepository;

import java.util.List;

@Service("serviceEvent")
public class EventServiceImp implements IEventService {

    private final EventRepository eventRepository;
    private final UserRepository userRepository;

    @Autowired
    public EventServiceImp(EventRepository eventRepository,UserRepository userRepository) {
        this.eventRepository = eventRepository;
        this.userRepository=userRepository;
    }


    @Override
    public List<EventModel> getAll() {
        return eventRepository.findAll();
    }

    @Override
    public EventModel getById(Long id) {
        return eventRepository.findById(id).orElse(null);
    }


    @Override
    public EventModel save(EventModel eventModel) {
        return eventRepository.save(eventModel);
    }

    @Override
    public String byTicket(Long userId, Long eventId) {
        UserModel user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        EventModel event = eventRepository.findById(eventId).orElseThrow(() -> new ResourceNotFoundException("Event not found"));

        user.getEvents().add(event);
        event.getUsers().add(user);

        userRepository.save(user);
        eventRepository.save(event);
        return  "Ticket Comprado";
    }

    @Override
    public EventModel update(Long id,EventModel event) {
        EventModel eventDb = eventRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Evento no existe!"));
        eventDb.setName(event.getName());
        eventDb.setDayEvent(event.getDayEvent());
        eventDb.setTickes(event.getTickes());
        eventDb.setPrice(event.getPrice());

        return eventRepository.save(eventDb);
    }
}
