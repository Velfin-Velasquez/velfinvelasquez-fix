package prueba_tecnica_spring.service;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import prueba_tecnica_spring.exeptions.ResourceNotFoundException;
import prueba_tecnica_spring.models.PersonModel;
import prueba_tecnica_spring.models.SessionModel;
import prueba_tecnica_spring.models.UserModel;
import prueba_tecnica_spring.repository.PersonRepository;
import prueba_tecnica_spring.repository.SessionRepository;
import prueba_tecnica_spring.repository.UserRepository;
import prueba_tecnica_spring.util.ValidatorData;

/**
 * <b>Author:</b> Velfin Velasquez <br>
 * <b>Description:</b> This class implements the IUserService methods and interacts with the repository. <br>
 */
@Service("userService")
public class UserServiceImpl implements IUserService {
    private final UserRepository userRepository;
    private final SessionRepository sessionRepository;
    private final PersonRepository personRepository;

    @Autowired
    public UserServiceImpl(SessionRepository sessionRepository,PersonRepository personRepository,UserRepository userRepository) {
        this.personRepository = personRepository;
        this.userRepository = userRepository;
        this.sessionRepository=sessionRepository;
    }

    /**
     * This method returns a list of people from the database.
     * @return List of UserModel
     */
    @Override
    public List<UserModel> getAll() {
        return userRepository.findAll();
    }

    /**
     * This method returns a person from the database by their id
     * @param id Long
     * @return UserModel
     */
    @Override
    public UserModel getById(Long id) {
        UserModel userDb;
        userDb = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User no existe!"));
        return userDb;
    }

    /**
     * This method returns a person from the database by their username
     * @param username String
     * @return UserModel
     */
    @Override
    public UserModel getByUsername(String username) {
        UserModel userDb;
        try {
            userDb = userRepository.findByUsername(username);
            return userDb;
        }catch (Exception ex){
            throw new ResourceNotFoundException("User no existe!");
        }


    }

    /**
     * This method receives an email or a username of type string and if it exists, it closes the session of that user
     * @param userOrEmail String
     * @return UserModel
     */
    @Override
    public UserModel logout(String userOrEmail) {
        UserModel user = new UserModel();

        if (ValidatorData.isEmail(userOrEmail)) {
            user = userRepository.findByEmail(userOrEmail);
        } else {
            user = userRepository.findByUsername(userOrEmail);
        }

        if (user != null) {
            List<SessionModel> sessions = sessionRepository.findOpenSessionsByUserId(user.getId());
            // guardar la fecha del cierre de sesion
            for (SessionModel session : sessions) {
               session.setFechaCierre(new Date());
               sessionRepository.save(session);
            }
            user.setSession_active(false);
            return userRepository.save(user);
        } else {
            return null;
        }

    }

    /**
     * This method closes all sessions except those of the administrator.
     * @return String
     */
    @Override
    public String logoutAllUser() {
        List<UserModel> users = userRepository.findAll();
        for (UserModel userL : users) {
            boolean isAdmin = userL.getRoles().stream().anyMatch(role -> "ADMIN".equals(role.getName()));
            if (!isAdmin) {
                userL.setSession_active(false);
                userRepository.save(userL);
                List<SessionModel> sessions = sessionRepository.findOpenSessionsByUserId(userL.getId());

                // guardar la fecha del cierre de sesion
                for (SessionModel session : sessions) {
                    session.setFechaCierre(new Date());
                    sessionRepository.save(session);
                }
            }
        }
        return "proceso terminado!";
    }

    /**
     * this method receives the id of a PersonModel and a UserModel and registers a new user
     * @param id   Long id PersonModel
     * @param user UserModel
     * @return UserModel
     */
    @Override
    public UserModel save(Long id, UserModel user) {
        PersonModel personDb = personRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Persona no existe!"));

        if (userRepository.countUsersByPersonId(id) <= 0) {
            UserModel newUser = new UserModel();
            newUser.setEmail(user.getEmail());
            newUser.setPassword(user.getPassword());
            newUser.setUsername(user.getUsername());
            newUser.setPerson(personDb);
            return userRepository.save(newUser);
        }

        return null;

    }

    /**
     * Receives a user or email in string and returns a UserModel
     * @param userOrEmail String
     * @return UserModel
     */
    @Override
    public UserModel getUserByUserOrEmail(String userOrEmail){
        UserModel user = new UserModel();
        if (ValidatorData.isEmail(userOrEmail)) {
            user = userRepository.findByEmail(userOrEmail);
        } else {
            user = userRepository.findByUsername(userOrEmail);
        }
        return  user;
    }

}
