package prueba_tecnica_spring.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "events")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Future(message = "dayEvent No debe ser anterior a la fecha actual")
    @NotNull(message = "dayEvent es obligatoria")
    private Date dayEvent;

    @Positive(message = "No se aceptan valores negativos")
    @NotNull(message = "tickes es obligatoria")
    private Integer tickes;

    @Positive(message = "No se aceptan valores negativos")
    @NotNull(message = "price es obligatoria")
    private double price;

    @ManyToMany(mappedBy = "events")
    @JsonIgnoreProperties("events")
    private List<UserModel> users;

}
