package prueba_tecnica_spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import prueba_tecnica_spring.models.EventModel;

public interface EventRepository extends JpaRepository<EventModel, Long> { }
