<p align="center">
  <a href="https://spring.io/projects/spring-boot" target="blank"><img src="https://frontbackend.com/storage/tutorials/thymeleaf/spring-boot-logo.png" width="130" alt="Nest Logo" /></a>
</p>


# Para levantar la Api en modo desarrolo

1) ubicarse en la raiz del proyecto y ejecute


```bash
  ./mvnw spring-boot:run
```
    


## Para ver los endpints escriba en el navegador 
```
https://documenter.getpostman.com/view/14749617/2s946bCFC9
```

